import React from 'react';
import * as go from 'gojs';
import { ToolManager, Diagram } from 'gojs';
import { GojsDiagram } from 'react-gojs';
import './MyDiagram.css';
import SelectionDetails from './SelectionDetails';
import '../Images/gandalf.jpg'

const icons = {};

icons.coso = '../Images/gandalf.jpg';

class MyDiagram extends React.Component {
    nodeId = 0;

    constructor(props) {
        super(props);
        this.createDiagram = this.createDiagram.bind(this);
        this.nodeSelectionHandler = this.nodeSelectionHandler.bind(this);
        //this.updateNodeText = this.updateNodeText.bind(this);
        //this.onTextEdited = this.onTextEdited.bind(this);
        this.state = {
            selectedNodeKeys: [],
            icons: {},
            /*
                Aca se coloca los "usuarios iniciales"
            */ 
            model: {
                nodeDataArray: [{ key: 'Alpha', color: 'lightblue', icons: 'coso', iconWidth: 130 },
                                    { key: 'Beta', label: 'Beta', color: 'orange', source: 'image.png' },
                                    { key: 'Gamma', label: 'Gamma', color: 'lightgreen', source: 'image.png' },
                                    { key: 'Delta', label: 'Delta', color: 'pink', source: 'image.png' },
                                    { key: 'Omega', label: 'Omega', color: 'grey', source: 'image.png' },
                                    { key: 'Pepehhhhh', label: 'Pepehhhhh', color: 'lightgreen', source: 'image.png' },
                                    { key: 'Joseito', label: 'Joseito', color: 'lightgreen', source: 'image.png', id: 'prueba' },
                                    { key: 'Juanito', label: 'Juanito', color: 'lightgreen', source: 'image.png' },
                                    { key: 'Tu mama', label: 'Tu mama', color: 'lightgreen', source: 'image.png' },
                                    { key: 'Ragnar', label: 'Ragnar', color: 'lightgreen', source: 'image.png' },
                                    { key: 'Ubbe', label: 'Ubbe', color: 'lightgreen', source: 'image.png' }],
                linkDataArray: [{ from: 'Alpha', to: 'Beta' },
                                { from: 'Alpha', to: 'Gamma' },
                                { from: 'Beta', to: 'Delta' },
                                { from: 'Gamma', to: 'Omega' }]
            }
        };
    }

    componentWillMount(){
        this.setState({
            icons: {
                coso: "M9.30052 43.7165C3.63471 39.229 0 32.289 0 24.5C0 10.969 10.969 0 24.5 0C38.031 0 49 10.969 49 24.5C49 32.4823 45.1826 39.573 39.2738 44.0461C39.6251 42.7512 39.8126 41.3888 39.8126 39.9826C39.8126 33.4288 35.7404 27.826 29.9883 25.5665C32.0369 23.9135 33.3473 21.3823 33.3473 18.5451C33.3473 13.5649 29.3101 9.52772 24.3299 9.52772C19.3498 9.52772 15.3126 13.5649 15.3126 18.5451C15.3126 21.3823 16.6229 23.9135 18.6715 25.5665C12.9194 27.826 8.84728 33.4288 8.84728 39.9826C8.84728 41.27 9.0044 42.5207 9.30052 43.7165Z"
            }
        })
    }

    render() {
        return [
            <SelectionDetails key="selectionDetails" selectedNodes={this.state.selectedNodeKeys} />,
            <GojsDiagram
                key="gojsDiagram"
                diagramId="myDiagramDiv"
                model={this.state.model}
                createDiagram={this.createDiagram}
                className="myDiagram"
                onModelChange={this.modelChangeHandler}
            />
        ];
    }
    /*
        Logica para crear el diagrama
    */ 
    createDiagram(diagramId) {
        const $ = go.GraphObject.make;

        // Posicion y forma del diagrama
        const myDiagram = $(go.Diagram, diagramId, {
            initialContentAlignment: go.Spot.LeftCenter,
            layout: $(go.TreeLayout, {
                angle: 0,
                arrangement: go.TreeLayout.ArrangementVertical,
                treeStyle: go.TreeLayout.StyleLayered
            }),
            // Condiciones de "usabilidad"
            isReadOnly: false,
            allowHorizontalScroll: true,
            allowVerticalScroll: true,
            allowZoom: false,
            allowSelect: true,
            autoScale: Diagram.Uniform,
            contentAlignment: go.Spot.LeftCenter,
            TextEdited: this.onTextEdited
        });
        
        myDiagram.toolManager.panningTool.isEnabled = false;
        myDiagram.toolManager.mouseWheelBehavior = ToolManager.WheelScroll;

        // Estilos del template, e.g. Colores, tipo de rectangulos, etc.
        myDiagram.nodeTemplate = $(
            go.Node,
            'Auto',
            {
                selectionChanged: node => this.nodeSelectionHandler(node.key, node.isSelected)
            },
            // Editar Cuadrado
            $(go.Shape, 'RoundedRectangle', { strokeWidth: 0 }, new go.Binding('fill', 'color')),
            // Editar Texto
            $(go.TextBlock, {font: "24px Lato, sans-serif", margin: 8, editable: true}, new go.Binding('text', 'label')),
            // Editar Imagen
            $(go.Picture, { margin: 10, width: 50, height: 50, background: "url('../Images/gandalf.jpg')" }, new go.Binding("source")),
        );

        myDiagram.linkTemplate =
        $(go.Link, {
          toShortLength: -2,
          fromShortLength: -2,
          layerName: "Background",
          routing: go.Link.Orthogonal,
          corner: 15,
          fromSpot: go.Spot.RightSide,
          toSpot: go.Spot.LeftSide
        },
            new go.Binding("points").makeTwoWay(),
            // mark each Shape to get the link geometry with isPanelMain: true
            $(go.Shape, { isPanelMain: true, stroke: "#41BFEC"/* blue*/, strokeWidth: 10 },
            new go.Binding("stroke", "color")),
            $(go.Shape, { isPanelMain: true, stroke: "transparent", strokeWidth: 3, name: "PIPE", strokeDashArray: [20, 40] })
        );
        return myDiagram;
    }

    /* 
        Aca esta la logica si se modifica por parte del usuario los 
        rectangulos de los donantes, lo voy a comentar... Usar
        de ser necesario
    */
    /* 
    
    modelChangeHandler(event) {
        switch (event.eventType) {
            case ModelChangeEventType.Remove:
                if (event.nodeData) {
                    this.removeNode(event.nodeData.key);
                }
                if (event.linkData) {
                    this.removeLink(event.linkData);
                }
                break;
            default:
                break;
        }
    }

    addNode() {
        const newNodeId = 'node' + this.nodeId;
        const linksToAdd = this.state.selectedNodeKeys.map(parent => {
            return { from: parent, to: newNodeId };
        });
        this.setState({
            ...this.state,
            model: {
                ...this.state.model,
                nodeDataArray: [
                    ...this.state.model.nodeDataArray,
                    { key: newNodeId, label: newNodeId, color: getRandomColor() }
                ],
                linkDataArray:
                    linksToAdd.length > 0
                        ? [...this.state.model.linkDataArray].concat(linksToAdd)
                        : [...this.state.model.linkDataArray]
            }
        });
        this.nodeId += 1;
    }

    removeNode(nodeKey) {
        const nodeToRemoveIndex = this.state.model.nodeDataArray.findIndex(node => node.key === nodeKey);
        if (nodeToRemoveIndex === -1) {
            return;
        }
        this.setState({
            ...this.state,
            model: {
                ...this.state.model,
                nodeDataArray: [
                    ...this.state.model.nodeDataArray.slice(0, nodeToRemoveIndex),
                    ...this.state.model.nodeDataArray.slice(nodeToRemoveIndex + 1)
                ]
            }
        });
    }

    removeLink(linKToRemove) {
        const linkToRemoveIndex = this.state.model.linkDataArray.findIndex(
            link => link.from === linKToRemove.from && link.to === linKToRemove.to
        );
        if (linkToRemoveIndex === -1) {
            return;
        }
        return {
            ...this.state,
            model: {
                ...this.state.model,
                linkDataArray: [
                    ...this.state.model.linkDataArray.slice(0, linkToRemoveIndex),
                    ...this.state.model.linkDataArray.slice(linkToRemoveIndex + 1)
                ]
            }
        };
    }

    updateNodeText(nodeKey, text) {
        const nodeToUpdateIndex = this.state.model.nodeDataArray.findIndex(node => node.key === nodeKey);
        if (nodeToUpdateIndex === -1) {
            return;
        }
        this.setState({
            ...this.state,
            model: {
                ...this.state.model,
                nodeDataArray: [
                    ...this.state.model.nodeDataArray.slice(0, nodeToUpdateIndex),
                    {
                        ...this.state.model.nodeDataArray[nodeToUpdateIndex],
                        label: text
                    },
                    ...this.state.model.nodeDataArray.slice(nodeToUpdateIndex + 1)
                ]
            }
        });
    }

    */

    /* 
        Esta funcion es la que permite que cuando selecciones un "donante"
        puedas ver el detalle
    */
    nodeSelectionHandler(nodeKey, isSelected) {
        if (isSelected) {
            this.setState({
                ...this.state,
                selectedNodeKeys: [...this.state.selectedNodeKeys, nodeKey]
            });
        } else {
            const nodeIndexToRemove = this.state.selectedNodeKeys.findIndex(key => key === nodeKey);
            if (nodeIndexToRemove === -1) {
                return;
            }
            this.setState({
                ...this.state,
                selectedNodeKeys: [
                    ...this.state.selectedNodeKeys.slice(0, nodeIndexToRemove),
                    ...this.state.selectedNodeKeys.slice(nodeIndexToRemove + 1)
                ]
            });
        }
    }

    /*

    onTextEdited(e) {
        const tb = e.subject;
        if (tb === null) {
            return;
        }
        const node = tb.part;
        if (node instanceof go.Node) {
            this.updateNodeText(node.key, tb.text);
        }
    }
    */ 
}

export default MyDiagram;
